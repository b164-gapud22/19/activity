// number 3 and 4
const number = prompt('enter your number here');
const getCube = number ** 3;
console.log(`The cube of ${number} is ${getCube}`);

// number 5 and 6
const address = ['258', 'Washington Ave', 'NW', 'Califonia', '90011'];

const [lotNumber, avenue, city, state, postal] = address

console.log(`I live at ${lotNumber} ${avenue} ${city}, ${state} ${postal}`);

// number 7 and 8
const animal = {
	name: 'Birdie',
	specie: 'Eagle',
	weight: '10 kg',
	measurement: '20 inch'
};

const {name, specie, weight, measurement} = animal

console.log(`${name} is an ${specie}. He weighed ${weight} with a measurement of ${measurement}`);

// number 9,10 and 11
const listNumbers = [1, 2, 3, 4, 5];

	// Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers
listNumbers.forEach(number => {console.log(number)});

	// Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

const reduceNumber = listNumbers.reduce((x ,y) => {
	return x + y
});

console.log(reduceNumber);

// number 12 and 13

class dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new dog('Casia', '2', 'Pomeranian')

console.log(myDog)

